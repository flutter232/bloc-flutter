import 'dart:async';
import 'package:flutter/foundation.dart';

import './validators.dart';
import 'package:rxdart/rxdart.dart';

class Bloc extends Object with Validators {
  final _email = BehaviorSubject<String>();
  final _password = BehaviorSubject<String>();

  // Change Data
  Function(String) get changeEmail => _email.sink.add;
  Function(String) get changePassword => _password.sink.add;

  // Add data to stream
  Stream<String> get email => _email.stream.transform(validateEmail);
  Stream<String> get password => _password.stream.transform(validatePassword);
  Stream<bool> get submitValid =>
      CombineLatestStream.combine2(email, password, (a, b) => true);

  submit() {
    final validEmail = _email.value;
    final validPassword = _password.value;

    if (kDebugMode) {
      print('Email is $validEmail');
      print('Password is $validPassword');
    }
  }

  dispose() {
    _email.close();
    _password.close();
  }
}
