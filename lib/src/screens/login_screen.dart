import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../blocs/provider.dart';
import "../blocs/bloc.dart";

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of(context);
    return Container(
      margin: const EdgeInsets.all(20.0),
      child: Column(
        children: <Widget>[
          emailField(bloc),
          Container(margin: const EdgeInsets.only(top: 20.0)),
          passwordField(bloc),
          Container(margin: const EdgeInsets.only(top: 25.0)),
          submitButton(bloc),
        ],
      ),
    );
  }

  Widget emailField(Bloc bloc) {
    return StreamBuilder(
        stream: bloc.email,
        builder: (context, snapshot) {
          return TextFormField(
              decoration: InputDecoration(
                labelText: 'Email Address',
                hintText: 'you@example.com',
                icon: const Icon(Icons.email),
                errorText: snapshot.hasError ? snapshot.error.toString() : null,
              ),
              onChanged: bloc.changeEmail,
              keyboardType: TextInputType.emailAddress);
        });
  }

  Widget passwordField(Bloc bloc) {
    return StreamBuilder(
        stream: bloc.password,
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
          return TextFormField(
              decoration: InputDecoration(
                labelText: 'Password',
                hintText: 'Enter your password',
                icon: const Icon(Icons.lock),
                errorText: snapshot.hasError ? snapshot.error.toString() : null,
              ),
              onChanged: bloc.changePassword,
              obscureText: true,
              keyboardType: TextInputType.visiblePassword);
        });
  }

  Widget submitButton(Bloc bloc) {
    return StreamBuilder(
        builder: (context, snapshot) {
          return ElevatedButton.icon(
            onPressed: snapshot.hasError ? null : bloc.submit,
            icon: const Icon(Icons.lock_open),
            label: const Text("Login"),
            style: ElevatedButton.styleFrom(
                fixedSize: const Size.fromWidth(double.infinity),
                padding: const EdgeInsets.symmetric(vertical: 20.0),
                maximumSize: const Size.fromWidth(double.maxFinite),
                elevation: 8.0),
          );
        },
        stream: bloc.submitValid);
  }
}
