import 'package:flutter/material.dart';
import './blocs/provider.dart';
import './screens/login_screen.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Provider(
        child: MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Login BLOC",
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Login BLOC"),
        ),
        body: const LoginScreen(),
      ),
    ));
  }
}
